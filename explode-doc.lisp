(defpackage :spec-map/explode-doc
  (:nicknames :explode-doc)
  (:use :gt/full :cl-ppcre)
  (:export #:document))

(in-package :spec-map/explode-doc)
(in-readtable :curry-compose-reader-macros)

;;; Classes defining a document

;;; NOTE: if we find ourself doing functional-trees like things
;;; on this representation, adapt it to be consistent with functional-trees.
;;; In particular, any search or replacement operations that are like
;;; shadowed CL builtins, as defined as generics in functional-trees
;;; should be used instead of rolling custom functions.

(defclass document ()
  ((path
    :type (or null pathname)
    :accessor path
    :initarg :path
    :documentation "Pathname to the txt file")
   (raw-text
    :type string
    :accessor raw-text
    :initarg :raw-text
    :documentation "The raw contents of the file")
   (table-of-contents
    :accessor toc
    :accessor table-of-contents
    :initarg :toc
    :initarg :table-of-contents
    :documentation "Table of contents object for document"))
  (:documentation "Top level object for an exploded txt file"))

(defclass toc-entry ()
  ((section-number :initarg :section-number
                   :accessor section-number
                   :type string
                   :documentation "The identifier (numeric or not) of the section")
   (depth :initarg :depth
          :accessor depth
          :type (integer 0)
          :documentation "The nesting depth of this section, starting at 0")
   (title :initarg :title
          :accessor title
          :type string
          :documentation "Title of this section")
   (section :initarg :section
            :initform nil
            :accessor section
            :type (or null section)
            :documentation "The actual section that this entry refers to")
   (page-number :initarg :page-number
                :accessor page-number
                :initform nil
                :type (or null string)
                :documentation "Page number in the ToC for this section.")
   (line :initarg :line
         :accessor line
         :documentation "The line from the txt for this ToC entry")))

(defmethod slot-unbound ((class t) (obj toc-entry) (slot (eql 'depth)))
  (let ((sn (section-number obj)))
    (setf (slot-value obj slot) (count #\. sn :end (position #\Space sn)))))

(defclass section ()
  ((toc-entry :type (or string null)
              :initarg :toc-entry
              :accessor toc-entry
              :documentation "Line in the ToC (without page number).")
   (start :type integer
          :initarg :start
          :accessor start
          :documentation "Position in RAW-TEXT of the section.")
   (end :type integer
        :initarg :end
        :accessor end
        :documentation "Character beyond the last character in the section in RAW-TEXT."))
  (:documentation "A part of a document"))

(defclass table-of-contents (section)
  ((entries :initarg :entries
            :accessor entries
            :documentation "List of toc-entry objects")
   (table :type (or null hash-table)
          :accessor table
          :initarg :table
          :initform (make-hash-table :test #'equal)
          :documentation "Map from section ids to toc-entries"))
  (:documentation "Special section, the table of contents"))

(defmethod convert ((type (eql 'document)) (s stream) &key path)
  (convert 'document (read-stream-content-into-string s) :path path))
(defmethod convert ((type (eql 'document)) (p pathname) &key external-format (path p))
  (convert 'document (read-file-into-string p :external-format external-format)
           :path path))

(defmethod convert ((type (eql 'document)) (string string) &key path &allow-other-keys)
  (nest
   (let ((doc (make-instance 'document
                             :raw-text string
                             :toc (make-instance 'table-of-contents)
                             :path path))
         (pos 0)
         (len (length string))))
   (flet ((aver (cond &rest args)
            (unless cond (if args
                             (apply #'error args)
                             (error "Failed aver"))))))
   (flet ((next-line ()
            #+debug-doc-explode (format t "Enter next-line~%")
            (let ((p pos))
              (declare (ignorable p))
              (iter (while (< pos len))
                    (when (eql (aref string pos) #\Newline) (incf pos) (return))
                    (incf pos))
              #+debug-doc-explode (format t "Skip ~a" (subseq string p pos))))
          (prefixp (s)
            (eql (string< s string :start2 pos) (length s)))
          (scan-line ()
            (coerce
             (iter (aver (< pos len) "Len = ~a, pos = ~a" len pos)
                   (let ((c (aref string pos)))
                     (incf pos)
                     (when (eql c #\Newline)
                       (finish))
                     (collecting c)))
             'string))))
   ;; Find the table of contents
   (progn
     (iter (while (< pos len))
           #+debug-doc-explode (format t "string[~a] = ~a~%" pos (char-name (aref string pos)))
           (while (member (aref string pos) '(#\Newline #\ZERO_WIDTH_NO-BREAK_SPACE)))
           (incf pos))
     (aver (prefixp "Contents") "No Contents found at ~a" pos)
     (next-line))
   ;; Now read until we find an empty line
   (let ((toc-entries
           (iter (aver (< pos len) "pos = ~a, len = ~a" pos len)
                 ;; Read line
                 (let ((line (scan-line)))
                   (until (equal line ""))
                   (let* ((parts (split-sequence #\Tab line))
                          (section-number (car parts)))
                     (collecting
                      (ecase (length parts)
                        (2 (make-instance
                            'toc-entry
                            :section-number section-number
                            :title ""
                            :page-number (cadr parts)))
                        (3 (make-instance
                            'toc-entry
                            :section-number section-number
                            :title (cadr parts)
                            :page-number (caddr parts)))))))))))
   (let ((tab (table (toc doc))))
     (setf (entries (toc doc)) toc-entries)
     (iter (for te in toc-entries)
           (let ((s (section-number te)))
             (aver (null (gethash s tab)) "Already in ToC: ~a" s)
             (setf (gethash s tab) te))))
   (progn
     ;; Confirm that we can find the section headers
     (let ((pos pos) (replacement (concatenate 'string ":" (string #\Newline))))
       (iter (for te in toc-entries)
             (let* ((pat
                      (if (equal (title te) "")
                          (section-number te)
                          (concatenate 'string (section-number te) (string #\Tab) (title te))))
                    (pat (regex-replace-all ": " pat replacement))
                    (spos (search pat string :start2 pos)))
               (if spos
                   (setf pos spos)
                   (collecting te)))))
     doc)))
