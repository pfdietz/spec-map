(defpackage :spec-map/test
  (:use #:gt/full #:stefil+ #:spec-map/spec-map #:spec-map/compressive)
  (:shadowing-import-from #:spec-map/spec-map #:*tables*)
  (:shadowing-import-from
   #:software-evolution-library/software/tree-sitter
   #:c)
  (:shadowing-import-from
   #:software-evolution-library/software-evolution-library
   #:from-string)
  (:shadowing-import-from #:cl-string-match #:suffix-tree)
  (:export #:test))

(in-package :spec-map/test)
(in-readtable :curry-compose-reader-macros)

(define-constant +attach+
    (asdf:system-relative-pathname :spec-map "test/etc/attach.c"))

(define-constant +attach-element+
    '("attach request" :struct
      (("Protocol discriminator")
       ("Security header type")
       ("Attach request message identity" "Message type")
       ("EPS attach type")
       ("NAS key set identifier")
       ("EPS mobile identity")
       ("UE network capability")
       ("ESM message container")
       ("Old P-TMSI signature" "P-TMSI signature")
       ("Additonal GUTI" "EPS mobile identity")
       ("Last visited registered TAI" "Tracking area identity")
       ("DRX paraneter")
       ("MS network capability")
       ("Old location area identification" "Location area identification")
       ("TMSI status")
       ("Mobile station classmark 2")
       ("Mobile station classmark 3")
       ("Supported Codecs" "Supported Codec List")
       ("Additional update type")
       ("Voice domain preference and UE's usage setting")
       ("Device properties")
       ("Old GUTI type" "GUTI type")
       ("MS network feature support")
       ("TMSI based NRI container" "Network resource identifier container")
       ("T3324 value" "GPRS timer 2")
       ("T3412 extended value" "GPRS timer 3")
       ("Extended DRX parameters")
       ("UE additional security capability")
       ("UE status")
       ("Additional information requested")
       ("N1 UE network capability")
       ("UE radio capability ID availability")
       ("Requested WUS assistance information" "WUS assistance information")
       ("DRX parameter in NB-S1 mode" "NB-S1 DRX parameter")
       ))
  :test #'equal
  :documentation "Element for the ATTACH REQUEST message from 3GPP TS 24.301")

(define-constant +eps-attach-type-element+
    '("EPS attach type" :enum
      (("EPS attach" 1)
       ("combined EPS/IMSI attach" 2)
       ("EPS RLOS attach" 3)
       ("EPS Emergency attach" 6)
       ("reserved" 7)))
  :test #'equalp
  :documentation "Element for EPS ATTACH TYPE from 3GPP TS 24.301")

(defroot test)
(defsuite spec-map "Test suite for spec-map")

(define-constant +expected-attach-match1+
     '((("Protocol discriminator")
        "eps_protocol_discriminator_t protocoldiscriminator : 4;")
       (("Security header type") "security_header_type_t securityheadertype : 4;")
       (("Attach request message identity" "Message type") "message_type_t messagetype;")
       (("EPS attach type") "eps_attach_type_t epsattachtype;")
       (("NAS key set identifier") "NasKeySetIdentifier naskeysetidentifier;")
       (("EPS mobile identity") "eps_mobile_identity_t oldgutiorimsi;")
       (("UE network capability") "ue_network_capability_t uenetworkcapability;")
       (("ESM message container") "EsmMessageContainer esmmessagecontainer;")
       (("Old P-TMSI signature" "P-TMSI signature") "p_tmsi_signature_t oldptmsisignature;")
       (("Additonal GUTI" "EPS mobile identity") "eps_mobile_identity_t additionalguti;")
       (("Last visited registered TAI" "Tracking area identity")
        "tai_t lastvisitedregisteredtai;")
       (("MS network capability") "ms_network_capability_t msnetworkcapability;")
       (("Old location area identification" "Location area identification")
        "location_area_identification_t oldlocationareaidentification;")
       (("TMSI status") "tmsi_status_t tmsistatus;")
       (("Mobile station classmark 2") "mobile_station_classmark2_t mobilestationclassmark2;")
       (("Mobile station classmark 3") "mobile_station_classmark3_t mobilestationclassmark3;")
       (("Supported Codecs" "Supported Codec List") "supported_codec_list_t supportedcodecs;")
       (("Additional update type") "additional_update_type_t additionalupdatetype;")
       (("Voice domain preference and UE's usage setting")
        "voice_domain_preference_and_ue_usage_setting_t voicedomainpreferenceandueusagesetting;")
       (("Old GUTI type" "GUTI type") "guti_type_t oldgutitype;")
       (("MS network feature support") "ms_network_feature_support_t msnetworkfeaturesupport;")
       (("TMSI based NRI container" "Network resource identifier container")
        "network_resource_identifier_container_t networkresourceidentifiercontainer;")
       (("DRX parameter in NB-S1 mode" "NB-S1 DRX parameter")
        "drx_parameter_t drxparameter;"))
  :test #'equalp)

(define-constant +expected-attach-match2+
  '((("EPS attach type") "emm_proc_attach_type_t type;")
    (("NAS key set identifier") "ksi_t ksi;")
    (("UE network capability") "ue_network_capability_t ue_network_capability;")
    (("Last visited registered TAI" "Tracking area identity") "tai_t* last_visited_registered_tai;")
    (("MS network capability") "ms_network_capability_t* ms_network_capability;")
    (("Mobile station classmark 2") "MobileStationClassmark2* mob_st_clsMark2;")
    (("Additional update type") "additional_update_type_t additional_update_type;")
    (("Voice domain preference and UE's usage setting")
     "voice_domain_preference_and_ue_usage_setting_t* voicedomainpreferenceandueusagesetting;")
    (("Old GUTI type" "GUTI type") "guti_t* guti;")
    (("DRX parameter in NB-S1 mode" "NB-S1 DRX parameter")
     "drx_parameter_t* drx_parameter;"))
  :test #'equalp)

(declaim (special *files*))

(defixture attach
  (:setup
   (setf *lang* :c
         *tables* nil
         *files* (list +attach+))
   (load-files :files *files*)))

(deftest attach-structure ()
  (with-fixture attach
    (let ((m (match-element +attach-element+ *tables*)))
      (is (equal (length m) 2))
      (is (> (caar (car m)) 0.8))
      (is (> (caar (cadr m)) 0.4))
      (is (equalp (cadr (car m)) (car *files*)))
      (is (equalp (cadr (cadr m)) (car *files*)))
      (is (equal
           (mapcar (lambda (x) (list (car x) (cadr x)))
                   (remove-if (lambda (x) (< (caddr x) 0.4))
                              (cadr (caar m))))
           +expected-attach-match1+))
      (is (equal
           (mapcar (lambda (x) (list (car x) (cadr x)))
                   (remove-if (lambda (x) (< (caddr x) 0.4))
                              (cadr (caadr m))))
           +expected-attach-match2+))
      )))

(define-constant +expected-enum-match+
    '((("EPS attach" 1) "EMM_ATTACH_TYPE_EPS = 0")
      (("combined EPS/IMSI attach" 2)
       "EMM_ATTACH_TYPE_COMBINED_EPS_IMSI")
      (("EPS Emergency attach" 6)
       "EMM_ATTACH_TYPE_EMERGENCY")
      (("reserved" 7) "EMM_ATTACH_TYPE_RESERVED"))
  :test #'equal)

(deftest attach-enum ()
  (with-fixture attach
    (let ((m (match-element  +eps-attach-type-element+ *tables*)))
      (is (equal (length m) 1))
      (is (equal
           (mapcar (lambda (x) (list (car x) (cadr x)))
                   (cadaar m))
           +expected-enum-match+)))))

;;;  Compressive tests

(defsuite compressive "Tests for compressive")

(deftest normalize-for-compression-test ()
  (is (equal "ab1c$" (normalize-for-compression "a b.1 c")))
  (is (equal "abc" (normalize-for-compression "aBc" :last "")))
  (is (equal "aBc!" (normalize-for-compression "aBc" :case t :last #\!)))
  (is (equal "abcd" (normalize-for-compression " 1ab2C_d"
                                               :filter 'alpha-char-p
                                               :last "")))
  (is (equal "intx" (normalize-for-compression
                     (from-string (make-instance 'c) "int x;")
                     :last ""))))

(deftest compression-score-test ()
  (let ((tree (suffix-tree-for-compression "abcXabcdYabc")))
    (is (typep tree 'suffix-tree))
    (is (equal 0.0f0 (compression-score tree "PQR"))))
  (let ((tree (suffix-tree-for-compression "foofoo$")))
    (is (<= (abs (- 1.0566f0 (compression-score tree "foo"))) 0.0001f0))))

(deftest ukk-repeated-substrings-test ()
  (is (equal '(("aa" 5) ("aaa" 4) ("aaaa" 3) ("aaaaa" 2))
             (ukk-repeated-substrings
              (suffix-tree-for-compression "aaaaaa$"))))
  (is (equal '(("ba" 4) ("aba" 3) ("baba" 2) ("baa" 2) ("abaa" 2) ("aa" 2))
             (ukk-repeated-substrings
              (suffix-tree-for-compression "bababaabaa$")))))
