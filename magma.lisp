(defpackage :spec-map/magma
  (:use :gt/full :spec-map/spec-map)
  (:use :software-evolution-library/software/tree-sitter
   :software-evolution-library/software/parseable)
  (:import-from :software-evolution-library/software-evolution-library
   :genome :from-string :from-file :software)
  (:import-from :software-evolution-library/utility/range
   :begin :end :source-location->position)
  (:shadowing-import-from :software-evolution-library/view
                          :+color-RED+ :+color-GRN+ :+color-RST+)
  (:shadowing-import-from :software-evolution-library/components/file :original-path)
  (:shadowing-import-from :spec-map/spec-map #:*tables* #:*source-dir* #:*lang*)
  (:export))

(in-package :spec-map/magma)
(in-readtable :curry-compose-reader-macros)

;;; Things specific to applying spec-map to the Magma code base

(defparameter *source-dir* (or (osicat-posix:getenv "SEL_MAP_DIR")
                               "/home/pdietz/magma/"))
(defparameter *lang* 'c)
(defparameter *magma-dir* "/home/pdietz/magma/")

(defun all-magma-files ()
  (remove-if (lambda (p) (member "test" (pathname-directory p)))
             (source-files :source-dir *magma-dir* :lang :c)))

(defparameter *attach-files*
  (mapcar (lambda (s)
            (concatenate
             'string
             *magma-dir*
             "lte/gateway/c/oai/tasks/"
             s))
          '("nas/emm/msg/AttachRequest.h"
            "nas/emm/emm_proc.h"
            "nas/emm/Attach.c"
            "nas/emm/msg/AttachRequest.c"
            "nas/emm/msg/AttachRequest.h"
            "nas/emm/sap/emm_recv.c"
            "nas/emm/sap/emm_recv.h")))

(declaim (special *sw-list* *tables*))

;;; Elements from spec
;;;
;;; Struct element:   (<field name> [ <field type> ])
;;; (and same for a union element)
;;; Enum element:  (<enum value name> [ <enum value> ])  value as a string

(defparameter *attach-elements*
  '(("attach request" :struct
     (("Protocol discriminator")
      ("Security header type")
      ("Attach request message identity" "Message type")
      ("EPS attach type")
      ("NAS key set identifier")
      ("EPS mobile identity")
      ("UE network capability")
      ("ESM message container")
      ("Old P-TMSI signature" "P-TMSI signature")
      ("Additonal GUTI" "EPS mobile identity")
      ("Last visited registered TAI" "Tracking area identity")
      ("DRX paraneter")
      ("MS network capability")
      ("Old location area identification" "Location area identification")
      ("TMSI status")
      ("Mobile station classmark 2")
      ("Mobile station classmark 3")
      ("Supported Codecs" "Supported Codec List")
      ("Additional update type")
      ("Voice domain preference and UE's usage setting")
      ("Device properties")
      ("Old GUTI type" "GUTI type")
      ("MS network feature support")
      ("TMSI based NRI container" "Network resource identifier container")
      ("T3324 value" "GPRS timer 2")
      ("T3412 extended value" "GPRS timer 3")
      ("Extended DRX parameters")
      ("UE additional security capability")
      ("UE status")
      ("Additional information requested")
      ("N1 UE network capability")
      ("UE radio capability ID availability")
      ("Requested WUS assistance information" "WUS assistance information")
      ("DRX parameter in NB-S1 mode" "NB-S1 DRX parameter")
      ))
    ("EPS attach type" :enum
     (("EPS attach" 1)
      ("combined EPS/IMSI attach" 2)
      ("EPS RLOS attach" 3)
      ("EPS Emergency attach" 6)
      ("reserved" 7)))
    ("Additional update type" :enum
     (("no additional information" 0)
      ("SMS only" 1)))
    ("NAS Key Set Identifier" :octet 1)
    ("EPS mobile identity" :union
     (("guti"
       ("MCC digits" :range (1 3) :nibble)
       ("MNC digits" :range (1 3) :nibble)
       ("MME Group ID" :octet 2)
       ("MME Code" :octet 1)
       ("M-TMSI" :octet 4))
      ;; (("imsi" "imei") ...)
      ))
    ("ESM message container" :struct
     (("iei" :octet 1)
      ("length" :integer (0..65535))
      ("ESM message container contents" :octets (0 65535))))
    ("Old P-TMSI signature" :struct
     (("P-TMSI signature value" :octet 3)))
    ))

