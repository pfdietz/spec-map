SPEC-MAP
========

Build a mapping between regions of a specification and regions of an
implementation of that specification.
