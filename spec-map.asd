(defsystem :spec-map
    :author "Paul F. Dietz"
    :license "proprietary"
    :description "Construction of mappings from specifications to code"
    :long-description "A toolkit for building a mapping from information
derived from specifications to entities in a code base."
  :version "0.0.0"
  :depends-on (:spec-map/spec-map
               :spec-map/compressive
               :spec-map/explode-doc)
    :class :package-inferred-system
    :defsystem-depends-on (:asdf-package-system)
    :in-order-to ((test-op (load-op "spec-map/test")))
    :perform
    (test-op (o c) (symbol-call :spec-map/test '#:run-batch))
    )
