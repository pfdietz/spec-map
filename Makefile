# Set personal or machine-local flags in a file named local.mk
ifneq ("$(wildcard local.mk)","")
include local.mk
endif

PACKAGE_NAME = spec-map

LISP_DEPS = $(wildcard *.lisp)

include .cl-make/cl.mk
