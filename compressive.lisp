(defpackage :spec-map/compressive
  (:use #:gt/full #:spec-map/spec-map)
  (:shadowing-import-from
   #:software-evolution-library/software/parseable
   #:source-text #:ast)
  (:shadowing-import-from
   #:software-evolution-library/software-evolution-library
   #:software #:genome)
  (:shadowing-import-from
   #:cl-string-match
   #:suffix-tree
   #:suffix-tree-root #:suffix-tree-str #:suffix-tree-nodes-count
   ;; #:suffix-node
   #:suffix-node-get-child
   #:ukk-node
   #:ukk-node-start
   #:ukk-node-end
   #:ukk-node-children
   #:ukk-node-suffix
   #:build-suffix-tree-ukkonen
   #:+infinity+)
  (:export #:suffix-tree-for-compression
           #:normalize-for-compression
           #:compression-score
           #:ukk-repeated-substrings)
  (:documentation "Compressive similarity of specs and code"))

(in-package :spec-map/compressive)
(in-readtable :curry-compose-reader-macros)

(declaim (inline ukk-node-size))
(defun ukk-node-size (node)
  (ukk-node-suffix node))

(defgeneric fix-ukk-nodes (tree)
  (:documentation "Replace the SUFFIX field of UKK-NODES with
the size of their substrees.  Also, replace END that is +INFINITY+
with the actual end of the tree's string.")
  (:method ((tree suffix-tree))
    (let ((len1 (1- (length (suffix-tree-str tree)))))
      (labels ((fix (node)
                 (when (eql (ukk-node-end node) +infinity+)
                   (setf (ukk-node-end node) len1))
                 (let ((c (ukk-node-children node)))
                   (cond
                     (c
                      (mapc #'fix c)
                      (setf (ukk-node-suffix node)
                            (reduce #'+ c :key #'ukk-node-suffix)))
                     (t
                      (setf (ukk-node-suffix node) 1))))))
        (fix (suffix-tree-root tree))))
    tree))

(defun ukk-repeated-substrings (tree)
  "Given a Ukkonen suffix tree TREE, find all the maximal repeated
substrings of rep > 1.  Report only the substrings that are not contained
in a larger substring of the same rep count.  Return a list of lists (<str>
<repcount>), sorted into nonincreasing order of rep count."
  (let ((result nil))
    (labels ((walk (len prefix node)
               (when (ukk-node-children node)
                 (unless (<= len 1)
                   (push (list prefix (ukk-node-size node)) result))
                 (dolist (c (ukk-node-children node))
                   (let* ((start (ukk-node-start c))
                          (end (ukk-node-end c))
                          (prefix (cons (list start end) prefix)))
                     (walk (+ len (- end start) 1) prefix c))))))
      (walk 0 nil (suffix-tree-root tree)))
    (mapcar (lambda (e)
              (destructuring-bind (pairs rep) e
                (list (mapconcat (lambda (p)
                                   (subseq (suffix-tree-str tree)
                                           (car p) (1+ (cadr p))))
                                 (reverse pairs) "")
                      rep)))
            (sort result #'> :key #'cadr))))

(defgeneric suffix-tree-longest-prefix (tree str &key start end)
  (:documentation "Return the length of the longest prefix of STR starting
at START that is in TREE, along with its rep count, or 0 and root size if it is
not in the tree."))

(defmethod suffix-tree-longest-prefix
    ((tree suffix-tree) (str simple-string) &key (start 0) (end nil))
  (unless end (setf end (length str)))
  (assert (<= 0 start end (length str)))
  (let ((node (suffix-tree-root tree))
        (s1 (suffix-tree-str tree))
        (i start)
        (len 0))
    (if (eql start end)
        (values 0 (ukk-node-size node))
        (loop
          (unless (< i end)
            (return (values len (ukk-node-size node))))
          (let* ((chr (aref str i))
                 (child (suffix-node-get-child tree node chr)))
            ;; If null, no match, so return what we have
            (unless child
              (return (values len (ukk-node-size node))))
            (assert (eql chr (aref s1 (ukk-node-start child))))
            ;; Find mismatch between segment of s1 on node
            ;; and str
            (setf node child)
            #+debug-compression
            (format t "i = ~a, len = ~a, start-node = ~a, end-node = ~a~%" i len
                      (ukk-node-start node) (ukk-node-end node))
            (let* ((end2 (if (eql (ukk-node-end node) +infinity+)
                             (length s1)
                             (1+ (ukk-node-end node))))
                   (m (mismatch str s1 :start1 i
                                       :end1 end
                                       :start2 (ukk-node-start node)
                                       :end2 end2)))
              #+debug-compression
              (format t "m = ~a~%" m)
              (cond
                ((null m)
                 ;; Matched the entire string
                 (return (values (- end start) (ukk-node-size node))))
                ((= (- m i) (- end2 (ukk-node-start node) ))
                  ;; We used all of the node
                 (incf len (- m i))
                 (setf i m))
                (t
                 (incf len (- m i))
                 (return (values len (ukk-node-size node)))))))))))

(defgeneric compression-score (tree str)
  (:documentation "Compute the compression score of STR relative to TREE.
Roughly, this is the sum of (size of word) x log(freq of word) over the
maximal words from TREE found in STR, divided by the length of STR"))

(defmethod compression-score ((tree suffix-tree) (str simple-string))
  (nest
   (let ((len (length str))
         (acc 0.0f0)
         (i 0)))
    (if (eql len 0) 0.0f0)
    (loop
      (when (= i len)
        (return (coerce (/ acc len) 'single-float)))
      (multiple-value-bind (l rep)
          (suffix-tree-longest-prefix tree str :start i)
        #+debug-compression
        (format t "l = ~a, rep = ~a~%" l rep)
        (case l
          ((0 1) (incf i))
          (t
           (incf acc (* (1- l) (log (1+ rep) 2.0f0)))
           (incf i l)))))))

(defgeneric suffix-tree-for-compression (str &key case filter last)
  (:documentation "Compute the suffix tree of STR, normalizing it first")
  (:method ((str t) &rest args)
    (~>> (apply #'normalize-for-compression str args)
         (build-suffix-tree-ukkonen)
         (fix-ukk-nodes))))

(defgeneric normalize-for-compression (str &key case filter last)
  (:documentation "Put string into normal form for compressive similarity")
  (:method ((sw software) &rest args)
    (apply #'normalize-for-compression (genome sw) args))
  (:method ((ast ast) &rest args)
    (apply #'normalize-for-compression (source-text ast) args))
  (:method ((str string) &key case (filter #'alphanumericp) (last #\$))
    (~>> (if case str (string-downcase str))
         (remove-if-not filter)
         (concatenate 'string _ (string last)))))
