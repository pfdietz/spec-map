(defpackage :spec-map/spec-map
  (:nicknames :spec-map)
  (:use #:software-evolution-library/software-evolution-library
   #:software-evolution-library/software/tree-sitter
   #:software-evolution-library/software/parseable
   #:software-evolution-library/utility/range
   #:software-evolution-library/components/file
   #:spec-map/keywords
   #:gt/full)
  (:import-from #:functional-trees)
  (:import-from #:osicat)
  (:import-from #:alexandria #:required-argument)
  (:import-from #:serapeum #:~>>)
  (:shadowing-import-from #:software-evolution-library/software/parseable
                          #:ast #:children #:parseable)
  (:shadowing-import-from #:software-evolution-library/view
                          #:+color-RED+ #:+color-GRN+ #:+color-RST+)
  (:shadowing-import-from #:software-evolution-library/command-line
                          #:create-software)
  (:export #:source-files
           #:load-files
           #:match-element
           #:strings-of-ast #:reduce-strings
           #:*lang* #:make-all-string-tuples
           #:index-string-tuples-by-string
           #:similarity
           #:jaccard-similarity
           #:similarity-alist
           #:reduce-string-to-name
           #:similar-apropos)
  (:documentation "Top level package for the SEL-MAP system"))

;; For now, this is working code
;; Move to appropriate files later

(in-package :spec-map)
(in-readtable :curry-compose-reader-macros)

(declaim (special *tables* *source-dir* *lang* *sw-list*))

(defun source-files (&key (lang *lang*) (source-dir *source-dir*))
  (let ((suffixes (lang-suffixes lang)))
    (loop for suffix in suffixes
          appending (directory (pathname (concatenate 'string source-dir "/**/*." suffix))))))

(defun lang-suffixes (&optional (lang *lang*))
  "Suffixes for files in one or more languages"
  (let ((langs (if (listp lang) lang (list lang))))
    (remove-duplicates
     (loop for l in langs
           appending (ecase l
                       ((:java sel/sw/ts:java) '("java"))
                       ((:c :h c) '("c" "h"))
                       ((:cpp :hpp cpp) '("cpp" "hpp" "c" "h"))
                       ((:go :golang golang) '("go"))
                       ((:rust rust) '("rst"))
                       ((:js :javascript javascript) '("js"))))
     :test #'equal)))

(defun make-class (lang)
  (ecase lang
    ((:java java) 'java)
    ((:c :h c) 'c)
    ((:cpp :hpp cpp) 'cpp)
    ((:go :golang golang) 'golang)
    ((:rust rust) 'rust)
    ((:js :javascript javascript) 'javascript)))

(defun load-files (&key files (lang  *lang*))
  (setf *sw-list* (mapcar {create-software _ :language (make-class lang)}
                          files))
  (setf *tables* (make-tables-for-sw *sw-list*)))

(defgeneric make-tables-for-sw (sw)
  (:method ((sw software))
    (list (list sw (index-string-tuples-by-string
                    (make-all-string-tuples sw)))))
  (:method ((sw list))
    (reduce #'append sw :key #'make-tables-for-sw :initial-value nil)))

;;; Collect the strings from a genome

(defgeneric strings-of-ast (ast)
  (:method ((ast string)) (list ast))
  (:method ((ast list))
    (reduce #'nconc ast :key #'strings-of-ast
                        :from-end t
                        :initial-value nil))
  (:method ((ast ast))
    (strings-of-ast (children ast)))
  (:method ((ast interleaved-text))
    (append (interleaved-text ast)
            (strings-of-ast (children ast))))
  (:method ((ast t)) nil))

(defun base-char-p (c)
  (typep c 'base-char))

(defun normalize-string (string)
  (etypecase string
    (simple-base-string string)
    (base-string (coerce string 'simple-base-string))
    (string
     (cond
       ((every #'base-char-p string)
        (coerce string 'simple-base-string))
       ((typep string 'simple-string) string)
       (t (coerce string 'simple-string))))))


;;; Reducing a set of strings produced by strings-of-ast

(defun latin-alpha-char-p (c)
  (position c "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"))

(defun latin-alpha-upper-case-p (c)
  (position c "ABCDEFGHIJKLMNOPQRSTUVWXYZ"))

(defun reduce-strings (string-list &key lang)
  (~>>
   string-list
   (cl:mapcar #'normalize-string)
   (cl:mapcar #'trim-whitespace)
   (cl:remove-if-not (lambda (str) (find-if #'latin-alpha-char-p str)) )
   (cl:mapcan (lambda (str)
                (split-sequence-if-not (lambda (c) (or (eql c #\_)
                                                       (alphanumericp c)))
                                       str)))
   (cl:remove "" _ :test #'equal)
   (cl:remove-if (lambda (x) (is-lang-keyword x lang)))))

(defun reduce-string-to-name (str)
  (remove-if-not #'latin-alpha-char-p str))

(defparameter *soft* nil "Default software object")

(defstruct string-tuple
  (str (required-argument "STRING-TUPLE requires STR") :type string)
  (ast (required-argument "STRING-TUPLE requires AST"))
  (range (required-argument "STRING-TUPLE requires RANGE") :type source-range)
  (sw nil :type (or software nil)))

(defgeneric make-all-string-tuples (val &key sw lang)
  (:documentation "Create all the string tuples of a SW object")
  (:method ((l list) &rest args)
    (mappend (lambda (x) (apply #'make-all-string-tuples x args)) l))
  (:method ((x parseable) &rest args &key (sw x) &allow-other-keys)
    (apply #'make-all-string-tuples (genome x) :sw sw args))
  (:method ((x ast) &key (sw nil) (lang :cpp))
    (let ((source-ranges (ast-source-ranges x)))
      (iter (for (a . source-range) in source-ranges)
        (nconcing
         (iter (for str in (interleaved-text a))
           (nconcing
            (iter (for s in (reduce-strings (list str) :lang lang))
              (collecting (make-string-tuple :ast a :str s :range source-range :sw sw))))))))))

(defun index-string-tuples-by-string (string-tuples &key case)
  "Produce a hash table mapping the strings of STRING-TUPLES to a string-info
object for that string"
  (let ((table (make-hash-table :test (if case 'equal 'equalp))))
    (iter (for tuple in string-tuples)
      (let* ((s (string-tuple-str tuple))
             (si (gethash s table)))
        (unless si
          (setf si (make-instance 'string-info :val s))
          (setf (gethash s table) si))
        (incf (string-info-count si))
        (push tuple (string-info-tuples si))))
    table))

;;; Count
;;    (sort (to-count-alist s :test #'equal) #'string< :key #'car)))

(defun to-count-alist (list &key (test #'eql))
  (let ((table (make-hash-table :test test)))
    (loop for s in list
          do (incf (gethash s table 0)))
    (loop for s in list
          append
          (let ((c (gethash s table)))
            (when (> c 0)
              (progn
                (setf (gethash s table) 0)
                (list (cons s c))))))))

(defclass names-info ()
  ((count-alist :initarg :count-alist :initform nil
                :accessor count-alist
                :documentation "Alist mapping strings to non-negative integers")
   (software :initarg :software
             :accessor software
             :documentation "Software object that has these names")))

(defmethod print-object ((obj names-info) s)
  (if *print-readably*
      (call-next-method)
      (print-unreadable-object (obj s :type t :identity t) )))

(defclass string-info ()
  ((val
    :initarg :value :initarg :string-val :initarg :val
    :accessor string-info-val
    :type string
    :documentation "The string that is the value")
   (count
    :initarg :count :initarg :string-count
    :accessor string-info-count
    :type (integer 0)
    :initform 0
    :documentation "Number of occurences of the string")
   (tuples
    :initarg :tuples
    :initform nil
    :accessor string-info-tuples
    :documentation "Mapping from SW object to list of string-tuples")
   (bigram-pair
    :initform nil
    :accessor string-info-bigram-pair
    :documentation "Slot to cache bigrams")
   (initials
    :accessor string-info-initials
    :documentation "Either NIL or a string that is the leading initials of the words of the strings")
   )
  (:documentation "Object representing the names in software objects"))

(defun initials-of-string (str)
  "Find the leading letters of words, or NIL if cannot reasonably do it"
  (let ((parts (split-sequence-if-not #'latin-alpha-char-p str :remove-empty-subseqs t)))
    (if (>= (length parts) 3)
        (map 'string (lambda (s) (elt s 0)) parts)
        (let ((parts (split-sequence-if-not #'latin-alpha-upper-case-p str
                                            :remove-empty-subseqs t)))
          (if (>= (length parts) 3)
              (map 'string (lambda (s) (elt s 0)) parts)
              nil)))))

(defmethod slot-unbound ((class t) (obj string-info) (slot-name (eql 'initials)))
  (setf (slot-value obj 'initials)
        (initials-of-string (string-info-val obj))))

(defmethod print-object ((obj string-info) s)
  (if *print-readably*
      (call-next-method)
      (print-unreadable-object (obj s :type t)
        (format s "\"~a\"" (string-info-val obj)))))

(defgeneric string-info-bigrams (si &key case)
  (:method ((str string) &key case)
    (bigrams str :case case))
  (:method ((si string-info) &key case)
    (let ((p (string-info-bigram-pair si)))
      (when (null p)
        (setf p (setf (string-info-bigram-pair si) (cons nil nil))))
      (if case
          (or (cdr p)
              (setf (cdr p) (bigrams (string-info-val si) :case t)))
          (or (car p)
              (setf (car p) (bigrams (string-info-val si))))))))

(defclass string-range ()
  ((sw :accessor string-range-sw
       :initarg :sw
       :documentation "The SW object where the string is located")
   (range :accessor string-range
          :initarg :pos
          :type source-range
          :documentation "The source range of the string in SW"))
  (:documentation "A source range containing this string"))

(defmethod print-object ((obj string-range) s)
  (if *print-readably*
      (call-next-method)
      (print-unreadable-object (obj s :type t)
        (format s "~a" (string-range obj)))))

(defgeneric make-names-info (software &key &allow-other-keys)
  (:documentation "Produce an object representing the name information in an ast"))

(defmethod make-names-info ((sw parseable) &key)
  (make-instance 'names-info
                 :count-alist (to-count-alist (reduce-strings (strings-of-ast (genome sw))))
                 :software sw))

(let* ((chars "0123456789abcdefghijklmnopqrstuvwxyz_")
       (nchars (length chars))
       (radix 64)
       (case-chars "0123456789abcdefghijklmnopqrstuvwxyABCDEFGHIJKLMNOPQRSTUVWXYZ_")
       (case-nchars (length case-chars)))
  (assert (<= nchars radix))
  (assert (<= case-nchars radix))

  (defun bigram-char-code (c)
    (position (char-downcase c) chars))

  (defun bigram-char-code-case (c case)
    (position (if case c (char-downcase c)) chars))

  (defun bigram-to-code (c1 c2 case)
    (let ((code1 (bigram-char-code-case c1 case))
          (code2 (bigram-char-code-case c2 case)))
      (declare (type (or null (and fixnum (integer 0))) code1 code2))
      (when (and code1 code2)
        (the fixnum (+ (the fixnum (* radix code1)) code2)))))

  (defun combine-codes (c1 c2)
     (declare (type (and fixnum (integer 0)) c1 c2))
    (the fixnum (+ (the fixnum (* radix c1)) c2)))

  (defun code-to-bigram (code)
    (declare (type (and fixnum (integer 0)) code))
    (multiple-value-bind (q r) (floor code radix)
      (values (elt chars q) (elt chars r)))))

(defparameter *bigrams-hash*
  (cons (make-hash-table :test #'equal)
        (make-hash-table :test #'equal)))

(defun bigrams (str &key case)
  "Compute the bigram count of a string"
  (declare (type string str))
  (setf str (remove-if-not #'alphanumericp str))
  (let* ((table (if case (car *bigrams-hash*)
                    (cdr *bigrams-hash*))))
    (or (gethash str table)
        (let ((len (length str))
              (bigram-alist nil))
          (when (> len 1)
            (let ((code2 (bigram-char-code-case (char str 0) case))
                  (code1 0))
              (loop for j from 1 below len
                    do (progn
                         (setf code1 code2)
                         (setf code2 (bigram-char-code-case (char str j) case))
                         (let ((code (combine-codes code1 code2)))
                           (let ((p (assoc code bigram-alist)))
                             (if p (incf (cdr p))
                                 (push (cons code 1) bigram-alist)))))))
            (assert bigram-alist () "NIL alist on ~a" str)
            (setf (gethash str table)
                  (sort bigram-alist #'< :key #'car)))))))

(defun is-integer-ranged-alist (alist)
  (every (lambda (p) (and (consp p) (integerp (cdr p)))) alist))

(defgeneric similarity (x y &key case)
  (:method (x y &key case) (jaccard-similarity x y :case case))
  (:method ((x string) y &key case)
    (max (if-let ((initials (initials-of-string x)))
           (jaccard-similarity initials y :case case)
           0.0)
         (jaccard-similarity x y :case case)))
  (:method ((x string-info) y &key case)
    (max (if-let ((initials (string-info-initials x)))
           (jaccard-similarity initials y :case case)
           0.0)
         (jaccard-similarity x y :case case))))

(defgeneric jaccard-similarity (ba1 ba2 &key case)
  (:documentation "Jaccard similarity of two count alists of integers, sorted by key")
  (:method ((ba1 string) (ba2 string) &key case)
    (jaccard-similarity (bigrams ba1 :case case) (bigrams ba2 :case case)))
  (:method ((ba1 string) (ba2 t) &key case)
    (jaccard-similarity (bigrams ba1 :case case) ba2 :case case))
  (:method ((ba1 t) (ba2 string) &key case)
    (jaccard-similarity ba1 (bigrams ba2 :case case) :case case))
  (:method ((si string-info) (ba2 t) &key case)
    (jaccard-similarity (string-info-bigrams si :case case) ba2 :case case))
  (:method ((ba1 t) (si string-info) &key case)
    (jaccard-similarity ba1 (string-info-bigrams si :case case) :case case))
  (:method ((ba1 cons) (ba2 null) &key case) (declare (ignore case)) 0.0f0)
  (:method ((ba1 null) (ba2 cons) &key case) (declare (ignore case)) 0.0f0)
  (:method ((ba1 list) (ba2 list) &key case)
    (declare (ignore case))
    (assert (is-integer-ranged-alist ba1))
    (assert (is-integer-ranged-alist ba2))
    (let ((total 0)
          (overlap 0))
      (loop while (and ba1 ba2)
            do (let ((c1 (car ba1))
                     (c2 (car ba2)))
                 (cond
                   ((< (car c1) (car c2))
                    (incf total (cdr c1))
                    (pop ba1))
                   ((> (car c1) (car c2))
                    (incf total (cdr c2))
                    (pop ba2))
                   (t
                     (let ((v1 (cdr c1))
                           (v2 (cdr c2)))
                       (incf total (max v1 v2))
                       (incf overlap (min v1 v2))
                       (pop ba1)
                       (pop ba2))))))
      (loop while ba1 do (incf total (cdr (pop ba1))))
      (loop while ba2 do (incf total (cdr (pop ba2))))
      ;; (format t "overlap = ~a, total = ~a~%" overlap total)
      (if (eql total 0)
          1.0f0
          (coerce (/ overlap total) 'single-float)))))

(defgeneric jaccard-subsimilarity (ba1 ba2 &key case)
  (:documentation
  "Like jaccard-similarity, but consider only BA1 when computing the total
This is approximately 'is ba1 like a part of ba2?'")
  (:method ((ba1 list) (ba2 list) &key case)
    (declare (ignore case))
    (let ((total 0)
          (overlap 0))
      (loop while (and ba1 ba2)
            do (let ((c1 (car ba1))
                   (c2 (car ba2)))
                 (cond
                   ((< (car c1) (car c2))
                    (incf total (cdr c1))
                    (pop ba1))
                   ((> (car c1) (car c2))
                    (pop ba2))
                   (t
                    (let ((v1 (cdr c1))
                          (v2 (cdr c2)))
                      (incf total (cdr c1))
                      (incf overlap (min v1 v2))
                      (pop ba1)
                      (pop ba2))))))
      (loop while ba1 do (incf total (cdr (pop ba1))))
      (if (eql total 0)
          1.0f0
          (coerce (/ overlap total) 'single-float)))))

(defgeneric similarity-alist (x collection &key fn)
  (:documentation "Compute an alist of <element> <similarity> pairs
for similarity of X to <element> via the similarity functio FN,
which must return a (real 0 1), sorted into non-increasing order
of similarity")
  (:method ((x t) (col list) &key (fn #'jaccard-similarity))
    (stable-sort (mapcar (lambda (e) (list e (funcall fn x e))) col) #'> :key #'cadr))
  (:method ((x t) (col hash-table) &key (fn #'jaccard-similarity))
    (let ((result nil))
      (maphash (lambda (k v) (push (list k (funcall fn x v) v) result)) col)
      (sort result #'> :key #'cadr))))


(macrolet ((b ()
             '(let* ((l1 (length s1))
                     (l2 (length s2))
                     (gap-val -0.9)
                     (ins-del-val -0.1)
                     (match-val 1.0)
                     ;; Best soln
                     (a (make-array (list (1+ l1) (1+ l2)) :element-type 'single-float
                                                           :initial-element 0.0f0))
                     ;; best soln with 1 or more deletes ending at this position
                     (del (make-array (list (1+ l1) (1+ l2)) :element-type 'single-float
                                                             :initial-element 0.0f0))
                     ;; best soln with 1 or more inserts ending at this position
                     (ins (make-array (list (1+ l1) (1+ l2)) :element-type 'single-float
                                                             :initial-element 0.0f0))
                     (lm (max l1 l2)))
               (if (eql lm 0)
                   1.0f0
                   (progn
                     ;; All deletes
                     (loop for i from 1 to l1
                        do (setf (aref a i 0)
                                 (setf (aref del i 0)
                                       (+ gap-val (* i ins-del-val)))))
                     ;; All inserts
                     (loop for j from 1 to l2
                          do (setf (aref a 0 j)
                                   (setf (aref ins 0 j)
                                         (+ gap-val (* j ins-del-val)))))

                     ;; The dynamic programming algorithm
                     (loop for i from 1 to l1
                           do (loop for j from 1 to l2
                                    do (let ((i1 (1- i))
                                             (j1 (1- j)))
                                         (setf (aref ins i j)
                                               (max (+ (aref ins i j1) ins-del-val)
                                                    (+ (aref a i j1) (+ ins-del-val gap-val))))
                                         (setf (aref del i j)
                                               (max (+ (aref ins i1 j) ins-del-val)
                                                    (+ (aref a i1 j) (+ ins-del-val gap-val))))
                                         (if (funcall test (aref s1 i1) (aref s2 j1))
                                             (setf (aref a i j)
                                                   (max (+ (aref a i1 j1) match-val)
                                                        (aref ins i j)
                                                        (aref del i j)))
                                             (setf (aref a i j)
                                                   (max (aref a i1 j1)
                                                        (aref ins i j)
                                                        (aref del i j)))))))
                     (/ (max 0.0f0 (aref a l1 l2)) (+ (min l1 l2)
                                                      (* (- (max l1 l2) (min l1 l2)) (- ins-del-val)))))))))
(defgeneric needleman-wunsch (s1 s2 &key test)
  (:documentation "compute the N-W distance of two sequences")
  (:method ((s1 vector) (s2 vector) &key (test #'eql)) (b))
  #+sbcl
  (:method ((s1 vector) (s2 vector) &key (test #'eql)) (b))
  (:method ((s1 string) (s2 string) &key (test #'eql)) (b))
  #+sbcl
  (:method ((s1 simple-base-string) (s2 simple-base-string) &key (test #'eql)) (b))
  #+sbcl
  (:method ((s1 sb-kernel:simple-character-string) (s2 sb-kernel:simple-character-string) &key (test #'eql)) (b))
  (:method ((s1 sequence) (s2 sequence) &key (test #'eql))
    (needleman-wunsch (coerce s1 'simple-vector) (coerce s2 'simple-vector) :test test))))

;;;;; Elements

(defclass spec-element ()
  ((name :type (or null string)
         :accessor name
         :initarg :name
         :initform nil
         :documentation "Name of the thing")
   (parts :type list
          :accessor parts
          :initarg :parts
          :initform nil
          :documentation "The parts of a spec element")
   )
  (:documentation "Objectified version of the elements from the spec"))

(defclass struct-element (spec-element) ())
(defclass enum-element (spec-element) ())
(defclass union-element (spec-element) ())
(defclass int-type-element (spec-element)
  ((num-bytes :accessor num-bytes
              :initform 1
              :initarg :num-bytes
              :documentation "Number of bytes in the int")
   (signed :accessor signed
           :initform nil
           :initarg :signed
           :documentation "Boolean indicating if the int is signed")))
(defun make-element (element)
  "Convert an element from lisp to object form"
  (unless (typep element '(cons string (cons keyword t)))
    (error "Not a valid element: ~a" element))
  (let* ((kind (cadr element))
         (obj (make-element-of-kind element kind)))
    (unless obj (error "Unrecognized element kind: ~s" kind))
    (setf (name obj) (car element))
    obj))

(defgeneric make-element-of-kind (element kind)
  (:documentation "Called from MAKE-ELEMENT to dispatch on KIND")
  (:method ((element t) (kind (eql :struct)))
    (make-instance 'struct-element :parts (caddr element)))
  (:method ((element t) (kind (eql :union)))
    (make-instance 'union-element :parts (caddr element)))
  (:method ((element t) (kind (eql :enum)))
    (make-instance 'enum-element :parts (caddr element)))
  (:method ((element t) (kind (eql :octet)))
    (make-instance 'int-type-element :bytes (caddr element))))

(defun find-similar-across-files (str &key (n 10) (tables *tables*))
  ;; Find the top N matches for STR in each file, as described in TABLES.
  ;; TABLES is produced by
  (loop for (sw table) in tables
        collect (list sw
                      (original-path sw)
                      (let ((alist (similarity-alist str table :fn #'similarity)))
                        (subseq alist 0 (min (length alist) n))))))

(defgeneric match-element (element tables)
  (:documentation "Find matches for ELEMENT in TABLES")
  (:method ((element cons) tables)
    (match-element (make-element element) tables))
  (:method ((element struct-element) tables)
    (match-element* element tables :filter #'find-and-confirm-enclosing-struct-definition))
  (:method ((element union-element) tables)
    (match-element* element tables :filter #'find-and-confirm-enclosing-union-definition))
  (:method ((element enum-element) tables)
    (match-element* element tables :filter #'find-and-confirm-enclosing-enum-definition))
  )

(defun match-element* (element tables &key (filter #'identity))
  (let ((defs (global-similarity
               (name element)
               :tables tables
               :n 10
               :filter filter)))
      ;; Now sort by score
      (let* ((previously-seen nil)
             (matchings
               (iter (for (nil nil ast sw path) in defs)
                 (unless (member ast previously-seen :test #'equal?)
                   (push ast previously-seen)
                   (collect (list (match-element-to-def element sw ast)
                                  path
                                  (source-text ast)))))))
        matchings)))

;; Refactor these

(defgeneric find-and-confirm-enclosing-definition (sw ast type)
  (:method ((sw c) (ast ast) (type t))
    (let ((def (enclosing-definition sw ast)))
      (when (equal (definition-name def) (source-text ast))
        (cond
          ((typep def 'c-type-definition)
           (when (typep (c-type def) type)
             (c-type def)))
          ((typep ast type) def))))))

(defgeneric find-and-confirm-enclosing-struct-definition (sw ast)
  (:documentation "Find and confirm that the enclosing definition for AST
is a structure definition, and has AST as a name.  REturn the structure definition,
or NIL if it is not confirmed.")
  (:method ((sw c) (ast ast))
    (find-and-confirm-enclosing-definition sw ast 'c-struct-specifier)))

(defgeneric find-and-confirm-enclosing-union-definition (sw ast)
  (:documentation "Find and confirm that the enclosing definition for AST
is a union definition, and has AST as a name.  REturn the union definition,
or NIL if it is not confirmed.")
  (:method ((sw c) (ast ast))
    (find-and-confirm-enclosing-definition sw ast 'c-union-specifier)))

(defgeneric find-and-confirm-enclosing-enum-definition (sw ast)
  (:documentation "Find and confirm that the enclosing definition for AST
is a enum definition, and has AST as a name.  REturn the enum definition,
or NIL if it is not confirmed.")
  (:method ((sw c) (ast ast))
    (find-and-confirm-enclosing-definition sw ast 'c-enum-specifier)))

(defun global-similarity (str &key (tables *tables*) (n 10) (filter nil))
  "Find the top N matches for STR in all files.  Returns a list
of triples: string, score, ast, sw object, path of sw object."
  (declare (type (integer 0) n))
  (let ((all (loop for (sw table) in tables
                   appending (let ((path (original-path sw)))
                               (loop for (s score e) in (similarity-alist str table :fn #'similarity)
                                     append (loop for st in (spec-map::string-info-tuples e)
                                                  for ast = (spec-map::string-tuple-ast st)
                                                  for range = (spec-map::string-tuple-range st)
                                                  for filtered-ast-or-nil = (if filter (funcall filter sw ast) e)
                                                  when filtered-ast-or-nil
                                                    collect (list s score filtered-ast-or-nil sw path range)))))))
    (setf all (sort all #'> :key #'cadr))
    (subseq all 0 (min n (length all)))))

(defun global-similarity-snippets (str &key (n 10) (filter nil))
  (let ((st-cache (make-hash-table)))
    (flet ((%sw-text (sw)
             (or (gethash sw st-cache)
                 (setf (gethash sw st-cache) (source-text (genome sw))))))
      (let ((sims (global-similarity str :n n)))
        (loop for (s score ast sw path tuple) in sims
              do (let ((txt (%sw-text sw)))
                   (when (or (null filter) (funcall filter sw ast))
                     (format t "~%------~%")
                     (format t "~a: ~%" path)
                     (format t "~a ~a~%---~%" s score)
                     (show-code-around sw (spec-map::string-tuple-range tuple)
                                       :txt txt))))
        (format t "~%------~%")))))

(defun show-code-around (sw source-range &key (color +color-GRN+) (around 200)
                            (txt (source-text (genome sw))))
  (let* ((start (source-location->position txt (begin source-range)))
         (end (source-location->position txt (end source-range))))
    (let ((s (max 0 (- start around)))
          (e (min (+ end around) (length txt))))
      (format t "~a~a~a~a~a"
              (subseq txt s start)
              color
              (subseq txt start end)
              +color-RST+
              (subseq txt end e))
      (fresh-line))))

;;; Matching to elements

(defgeneric match-to-field (spec-field-descriptor sw field-ast &key cutoff)
  (:method ((spec-field-descriptor cons) (sw t) (field-ast c-field-declaration) &key (cutoff 0.2f0))
    ;; Compute how well a spec and a field match
    (let* ((spec-field-name (car spec-field-descriptor))
           (spec-field-type (or (cadr spec-field-descriptor) spec-field-name))
           (field-type (source-text (c-type field-ast)))
           (field-name-ast (find-if {typep _ 'c-field-declaration} field-ast)))
      (assert field-name-ast () "Could not find name of field in ~a" (source-text field-ast))
      (let* ((field-name (source-text field-name-ast))
             (sim (max (similarity spec-field-name field-name)
                       (if (typep field-type 'c-primitive-type) 0.0f0
                           (similarity spec-field-type field-type)))))
        (if (<= sim cutoff) 0.0f0 sim))))
  (:method ((spec-enum-descriptor t) (sw t) (field-ast c-enumerator) &key (cutoff 0.2f0))
    (assert (consp spec-enum-descriptor))
    (let* ((spec-enum-name (car spec-enum-descriptor))
           (enum-name (source-text (c-name field-ast)))
           (sim (similarity spec-enum-name enum-name)))
      (if (<= sim cutoff) 0.0f0 sim))))

(defgeneric match-element-to-def (element sw ast)
  (:documentation "Compute a matching of an element to an AST.
Return list of two values: a match score, and a list of matches")
  (:method ((element t) (sw c) (ast t))
    (values 0.0f0 nil))
  (:method ((element struct-element) (sw c) (ast c-struct-specifier))
    (let* ((parts (parts element))
           (fields (remove-if-not {typep _ 'c-field-declaration} (when-let ((c (c-body ast))) (children c)))))
      (match-element-to-def* sw parts fields)))
  (:method ((element enum-element) (sw c) (ast c-enum-specifier))
    (let* ((parts (parts element))
           (fields (remove-if-not {typep _ 'c-enumerator} (when-let ((c (c-body ast))) (children c)))))
      (match-element-to-def* sw parts fields))))

(defun match-element-to-def* (sw parts fields)
  (let* ((m (length parts))
         (n (length fields))
         (a (make-array (list m n))))
    (loop for i from 0
          for p in parts
          do (loop for j from 0
                   for field in fields
                   do (setf (aref a i j)
                            (match-to-field p sw field))))
    (if (zerop (min m n))
        (values 0.0 nil)
        (let ((matching (maximum-matching a)))
          (list
           (if (<= m n)
               (combine-scores m (mapcar #'third matching))
               (combine-scores n (append (mapcar #'third matching)
                                         (make-list (- m n) :initial-element 0.0f0))))
           ;; Convert matching
           (loop for (i j score) in matching
                 when (> score 0.0f0)
                   collect (list (elt parts i)
                                 (source-text (elt fields j))
                                 score)))))))

(defun combine-scores (n scores)
  (if (zerop n)
      0.0f0
      (/ (reduce #'+ scores) (coerce n 'single-float))))

;; this is not quite perfect, but seems to work well
;; when there are obvious matchings
(defun maximum-matching (benefit)
  "Compute a maximum matching in a weighted bipartite graph"
  (assert (typep benefit '(array * (* *))))
  (let* ((m (array-dimension benefit 0))
         (n (array-dimension benefit 1))
         (result nil)
         (mleft m)
         (nleft n)
         (ninf most-negative-single-float))
    ;; use a greedy algorthm for now
    (iter
      (while (> mleft 0))
      (while (> nleft 0))
      ;; Find best unused weight
      (let ((best ninf)
            (ibest -1)
            (jbest -1))
        (iter (for i from 0 below m)
          (iter (for j from 0 below n)
            (when (> (aref benefit i j) best)
              (setf ibest i jbest j best (aref benefit i j)))))
        (while (> best ninf))
        (push (list ibest jbest (aref benefit ibest jbest)) result)
        (decf mleft)
        (decf nleft)
        (iter (for i from 0 below m)
          (setf (aref benefit i jbest) ninf))
        (iter (for j from 0 below n)
          (setf (aref benefit ibest j) ninf))))
    (sort (reverse result) #'< :key #'car)))

(defun similar-apropos (pattern &key (n 10) (filter #'identity))
  "Compute the N best matches of PATTERN to symbols.  Limit
the matching to symbols for which FILTER returns true."
  (let ((bl (bigrams (remove-if-not #'bigram-char-code pattern)))
        (result nil))
    (do-all-symbols (s)
      (when (funcall filter s)
        (let ((str (remove-if-not #'bigram-char-code (symbol-name s))))
          (when (> (length str) 1)
            (let ((sim (jaccard-similarity bl str)))
              (when sim
                (push (list s sim) result)))))))
    (setf result (sort result #'> :key #'cadr))
    (let ((c 0)
          (unique-results niL))
      (iter (for x in result)
            (while (< c n))
            (unless (member x unique-results :test #'equal)
              (push x unique-results)
              (incf c)))
      (nreverse unique-results))))

