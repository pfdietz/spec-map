typedef enum {
  EMM_ATTACH_TYPE_EPS = 0,
  EMM_ATTACH_TYPE_COMBINED_EPS_IMSI,
  EMM_ATTACH_TYPE_EMERGENCY,
  EMM_ATTACH_TYPE_RESERVED,
} emm_proc_attach_type_t;

typedef struct emm_attach_request_ies_s {
  bool is_initial;
  emm_proc_attach_type_t type;
  additional_update_type_t additional_update_type;
  bool is_native_sc;
  ksi_t ksi;
  bool is_native_guti;
  guti_t* guti;
  imsi_t* imsi;
  imei_t* imei;
  tai_t* last_visited_registered_tai;
  tai_t* originating_tai;
  ecgi_t* originating_ecgi;
  ue_network_capability_t ue_network_capability;
  ms_network_capability_t* ms_network_capability;
  drx_parameter_t* drx_parameter;
  bstring esm_msg;
  nas_message_decode_status_t decode_status;
  MobileStationClassmark2* mob_st_clsMark2; /* Mobile station classmark2 provided by the UE */
  voice_domain_preference_and_ue_usage_setting_t* voicedomainpreferenceandueusagesetting;
} emm_attach_request_ies_t;

typedef struct attach_request_msg_tag {
  /* Mandatory fields */
  eps_protocol_discriminator_t protocoldiscriminator : 4;
  security_header_type_t securityheadertype : 4;
  message_type_t messagetype;
  eps_attach_type_t epsattachtype;
  NasKeySetIdentifier naskeysetidentifier;
  eps_mobile_identity_t oldgutiorimsi;
  ue_network_capability_t uenetworkcapability;
  EsmMessageContainer esmmessagecontainer;
  /* Optional fields */
  uint32_t presencemask;
  p_tmsi_signature_t oldptmsisignature;
  eps_mobile_identity_t additionalguti;
  tai_t lastvisitedregisteredtai;
  drx_parameter_t drxparameter;
  ms_network_capability_t msnetworkcapability;
  location_area_identification_t oldlocationareaidentification;
  tmsi_status_t tmsistatus;
  mobile_station_classmark2_t mobilestationclassmark2;
  mobile_station_classmark3_t mobilestationclassmark3;
  supported_codec_list_t supportedcodecs;
  additional_update_type_t additionalupdatetype;
  guti_type_t oldgutitype;
  voice_domain_preference_and_ue_usage_setting_t voicedomainpreferenceandueusagesetting;
  ms_network_feature_support_t msnetworkfeaturesupport;
  network_resource_identifier_container_t networkresourceidentifiercontainer;
} attach_request_msg;
